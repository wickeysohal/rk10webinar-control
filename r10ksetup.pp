sshkey { 'bitbucket.org':
ensure => present,  
type => 'ssh-rsa',
target => "root/.ssh/known_hosts",
  key  => 'AAAAB3NzaC1yc2EAAAADAQABAAABgQCVk9NwvostvqhVT02oc/9w22JTkH5DwNfta4K9mV+4L5PNInXuoi64GXRrjyzEPuSATV48e30qk2d0uLFE2zzBn8VkDcfwkGQwn/hvZb5C7ShVjdWqqnlNLUxFYsJEbnMuIOgW81L/TdE+NV8bapiN8MoXfWIWhtHo0+5rxxAn8D/xwbBADSlB2BjfHa1hIEyFyznSfa/Tpy8AGScVNd1oMbjs1i4WORP9nmRO6slbl/GJS/EVFtioi6awHWt/j+gEdZ3dtLHhrjFBYBa/9gGP8j10XHnCf3Q4v4ORnSRR07HwAIDTrFO9IAL1YrNEcoORvP34PL7C5Vo37wGjowwtRnbapS30m/E6c1FhWIdURp71XLwYwgThNgG2ir5dnu/P7Dkacn8DuiKsFhhI/E2BsqeMpc36EkVLvk2th6cVYC6k4w3D2aZECo5ZovDxAofdg1dZtyL0nuy25WxjgWh44rtxFDPA22PJhfLgF1DBh2Xr5Zt41HV2eW0yvD2wDZc=',
} ->

class { 'r10k':
  version           => '2.1.1',
configfile => '/etc/puppetlabs/r10k/r10k.yaml'  
sources           => {
    'puppet' => {
      'remote'  => 'git clone git@bitbucket.org:wickeysohal/rk10webinar-control.git',
      'basedir' => $::settings::environmentpath,
      'prefix'  => false,
    }
  },
}
